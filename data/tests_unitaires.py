import unittest
import pandas as pd

from pretreatment import create_dict_meteo, add_meteo, reduire_bdd, ajout_transfos

class TestFunctions(unittest.TestCase):
    def setUp(self):
        # Créer des données de test pour les fonctions
        self.path_meteo = "meteo/meteo.csv"
        self.path_parkings = "parkings/parkings_sdb_treated_600.csv"

    def test_create_dict_meteo(self):
        # Test de create_dict_meteo
        dict_meteo = create_dict_meteo(self.path_meteo)
        self.assertIsInstance(dict_meteo, dict)

    def test_add_meteo(self):
        # Test de add_meteo
        parkings = pd.read_csv(self.path_parkings)
        add_meteo(self.path_parkings, self.path_meteo)

    def test_reduire_bdd(self):
        # Test de reduire_bdd
        df = pd.DataFrame({'surface_du_parking': [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]})
        df_reduit = reduire_bdd(df)
        self.assertEqual(len(df_reduit), 1)  # Vérifie si le nombre de lignes est correct

    def test_ajout_transfos(self):
        # Test de ajout_transfos
        df = pd.DataFrame({'col': [1, 2, 3, 4, 5]})
        df_modifie = ajout_transfos(df, proba=0.5)
        self.assertEqual(len(df_modifie), len(df)) 

if __name__ == '__main__':
    unittest.main()