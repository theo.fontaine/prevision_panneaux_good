# Prevision Panneaux

## Description 👨‍💻
Cet outil est destiné aux entreprises qui vendent des ombrières photovoltaïques. Il intervient pendant la phase de recherche de clients. Il propose les services suivants :
* la collecte automatique de données sur les parkings qui peuvent potentiellement être équipés. Ces données sont la localisation, la surface du parking, un score météo (qui dépend du nombre de jour de pluie moyen par année), la présence d'un transformateur proche et éventuellement le nom du parking. Celles-ci sont téléchargeables dans un fichier .csv (page `Carte`) ;
* la visualisation de ces données sur une carte (page `Carte`) ;
* une simulation de le gain potentiel du propriétaire du parking - argument de vente pour l'entreprise de panneaux photovoltaïques (page `Simulation`).

## Structure du projet 🏗
1. **data**
* Le dossier `data` contient toutes les bases de données traitées
2. **carte**
* Le dossier `carte` contient le module et les outils nécessaires permettant la convertion d'un .csv en carte .html
3. **page_app**
* Le dossier `page_app` contient les différentes pages contenue par l'application
4. **engagement_eco**
* Le dossier `engagement_eco` contient ...
5. **assets** et **working_docs**
* Les dossiers `assets` et `working_docs` contiennent les fichiers nécessaires à la réalisation de l'application

## Base de données 📊
Afin de mettre à bien notre projet, nous avons utilisé:
* l'emplacement des parkings en france d'OpenStreetMap
* l'emplacement des transformateurs en France
* les sites internet de grandes enseignes de distribution françaises

## API 🌍
L'utilisation de l'API https://public.opendatasoft.com/explore/dataset/donnees-synop-essentielles-omm/api/ a été primordial pour la constitution de données météo.

## Installation 🛠

**Se placer dans votre dossier destinataire**
```bash
cd VotreDossier
```

**Cloner le repository gitlab**
```bash
git clone https://gitlab-student.centralesupelec.fr/theo.fontaine/prevision_panneaux_good
```

**Créer un environnement virtuel**
```bash
python -m venv env
```

**Activer l'environnement virtuel**
```bash
source env/bin/activate ou $ source env/Scripts/activate
```

**Installer les librairies python nécessaires***
```bash
pip install -r requirements.txt
```

**Lancer l'application**
```bash
python app_main.py
```

**Ouvrir dans un navigateur la page (en général `http://127.0.0.1:8050/`)**


## Contribuer 👫
Si vous souhaitez contribuer à ce projet, n'hésitez pas à créer une nouvelle branche, à effectuer vos modifications et à soumettre un `pull`. Assurez-vous de suivre les normes et directives de codage du projet.

## Contacter les developpeurs 👥

Si vous avez des questions ou besoin de renseignement :

- **Théo DIEDA** : theo.dieda@student-cs.fr
- **Théo FONTAINE** : theo.fontaine@student-cs.fr
- **Martin POITIER** : martin.potier@student-cs.fr
- **Côme BENETEAU** : come.beneteau@student-cs.fr
- **Valentin ROQUES** : valentin.roques@student-cs.fr
- **Anatole DULON** : anatole.dulon@student-cs.fr