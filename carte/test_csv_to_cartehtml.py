import unittest
from unittest.mock import patch, Mock
from csv_to_cartehtml import converti_bdd_carte

class TestConvertiBddCarte(unittest.TestCase):

    @patch('pandas.read_csv')
    @patch('folium.Map')
    @patch('folium.MarkerCluster')
    @patch('folium.Marker')
    def test_converti_bdd_carte(self, mock_marker, mock_marker_cluster, mock_folium_map, mock_read_csv):
        mock_read_csv.return_value = Mock()
        mock_folium_map.return_value = Mock()
        mock_marker_cluster.return_value = Mock()
        mock_marker.return_value = Mock()

        seuil_meteo = 30
        seuil_surface_parking = 50
        avec_transfo = True

        converti_bdd_carte(seuil_meteo, seuil_surface_parking, avec_transfo)

        # Vérifie si les fonctions ont été appelées avec les bons arguments
        mock_read_csv.assert_called_once_with('/data/bdd_transfos.csv', encoding='latin-1', engine='python')
        mock_folium_map.assert_called_once_with(location=[46.603354, 2.457622], zoom_start=5)
        mock_marker_cluster.assert_called_once()
        mock_marker.assert_called()

if __name__ == '__main__':
    unittest.main()
