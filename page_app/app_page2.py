#Import des modules/bibliothèques
from dash import html, dcc
import dash_daq as daq

#Layout utilisé pour la page 2
tab_layout = html.Div([
    html.H2(children='Carte interactive', style={'margin-bottom': '15px'}),
    html.Div(children='Trouver les bons lieux pour implémenter des panneaux solaires en fonction des critères à droite de la carte', style={'textAlign': 'center'}),
    #######
    # Division de la page pour afficher la carte + critères en 70 / 30 %
    #######
    html.Div(style={'display': 'flex'}, children=[
        html.Iframe(id='map', srcDoc='', width='70%', height='550px', style={'float': 'left'}),
        html.Div(style={'width': '30%', 'float': 'right', 'padding': '20px'}, children=[
            html.Div([
                html.Label('Ensoleillement'),
                dcc.Slider( #Création du bouton glissière pour le critère ensoleillement
                    id='critere_ensoleillement',
                    min=0,
                    max=10,
                    step=1,
                    marks={i: str(i) for i in range(0, 11, 1)},
                    value=0,
                )
            ]),
            html.Div([
                html.Label('Taille minimum du parking : (en m²)  '),
                dcc.Input( #Création de la 'tchat box' pour input la valeur de la taille du parking
                    id='taille_parking_input',
                    type='number',
                    value=0,
                    min=0,
                    max=999999,
                    step=100,
                    style={'width': '70%', 'display': 'inline-block'}
                ),
                html.Div(id='budget-output', style={'margin-top': '10px', 'margin-bottom': '10px', 'font-size': '16px'})
            ]),
            html.Div([
                html.Label("Voir uniquement les parking avec tranformateur disponible"),
                html.Div([
                    daq.ToggleSwitch( #Création bouton présence ou non de transformateur
                        id='toggle-button-transfos',
                        value=False, 
                        style={'marginBottom': '10px'}
                    )
                ])
            ]),
            html.Div([
                html.Button("Télécharger la liste des parkings", id="btn-download-csv"), #Bouton de download de la bdd
                dcc.Download(id="download-csv")
            ]),
            html.Div(id='nb-parkings-affiches', style={'margin-top': '15px'})
        ]),
    ]),
])