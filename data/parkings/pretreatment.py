import geopandas as gpd
import pandas as pd

def add_infos( gdf : gpd.GeoDataFrame ) -> gpd.GeoDataFrame :
    
    '''
    rajoute l'aire, la latitude et la longitude du parking (précisément de son centre) et supprime les \
    latitudes et longitudes des sommets qui délimitent sa frontière.
    
    Args :
        gdf : la bdd à traiter.
    
    Returns :
        gdf : la bdd traitée.
    '''
    # La position des sommets du parking est donnée par leur latitude et leur longitude, mais la fonction
    # aire n'accepte en entrée que des coordonnées planes. Il faut donc effectuer la projection.
    
    # On définit la base de coordonnées actuelles (elle n'a pas été définie dans le fichier récupéré) comme
    # la base standard pour l'expression des latitudes et longitudes.
    gdf['geometry'].set_crs("EPSG:4326")
    
    # On peut désormais obtenir les coordonnées projetées. Attention, il faut utiliser un repère propre à la France.
    gdf ['geometry'] = gdf['geometry'].to_crs(2154)
    
    # On peut maintenant calculer l'aire et la position du centre de chaque parking.
    gdf['area'] = gdf.area
    gdf['centroid'] = gdf.centroid
    del gdf['geometry']
    
    # On recalcule la latitude et la longitude du centre du parking.
    gdf['centroid'] = gdf['centroid'].to_crs(4326)
    
    return gdf

def only_large_parkings( gdf : gpd.GeoDataFrame ) -> gpd.GeoDataFrame :
    
    '''
    Supprime les parkings d'aire inférieure à 1500 m^2.
    
    Args :
        gdf : la bdd à traiter.
    
    Returns :
        gdf : la bdd traitée.
    '''
    
    gdf = gdf.loc[ gdf['area'] >= 1500 ]
    return gdf

def delete_undesired_types( gdf : gpd.GeoDataFrame ) -> gpd.GeoDataFrame :
    
    '''
    Supprime les parkings souterrains. Supprime également les parkings à proximité de la voirie,
    car y installer des ombrières poseraient "des contraintes techniques et de sécurité". On fait
    ça pour éviter de proposer des parkings avec ces problèmes, mais il est probable que ce-faisant
    nous écartions des parkings sous le coup de la loi. Supprime aussi les parkings dont le type n'a
    pas été spécifié pour éviter les types non désirés.
    
    Args :
        gdf : la bdd à traiter.
    
    Returns :
        gdf : la bdd traitée.
    '''
    conserved_types = [ 'surface', 'multi-storey' ]
    gdf = gdf.loc[ gdf['parking'].isin( conserved_types ) ]
    return gdf

def better_format_for_lat_and_long (gdf : gpd.GeoDataFrame ) -> pd.DataFrame :
    
    '''
    Ajoute une colonne latitude et longitude du centre du parking, et supprime la colonne géométrie.
    
    Args :
        gdf : la bdd à traiter.
    
    Returns :
        gdf : la bdd traitée.
    '''
    
    centers = gdf['centroid']
    lats = []
    lons = []
    
    for center in centers:
        
        lat = center.xy[0][0]
        lats.append( lat )

        lon = center.xy[1][0]
        lons.append( lon )

    lats_c = pd.Series( data = lats, index = centers.index )
    lon_c = pd.Series( data = lons, index = centers.index )
    
    gdf['latitude'] = lats_c
    gdf['longitude'] = lon_c
    del gdf['centroid']
    
    return gdf

def change_columns_name( gdf : gpd.GeoDataFrame ) -> gpd.GeoDataFrame :
    '''
    Change the name of the dataset columns and reorder them such that the result follows the blueprint
    of later functions.
    
    Args :
        gdf : la bdd à traiter.
    
    Returns :
        gdf : la bdd traitée.
    '''
    new_names = { 'area' : 'surface_du_parking', 'latitude' : 'lat', 'longitude' : 'lon', 'name' : 'nom', 'parking' : 'type' }
    gdf.rename( columns = new_names, inplace=True )
    
    gdf = gdf[ [ 'nom', 'lat', 'lon', 'surface_du_parking', 'type', 'access'] ]
    
    return gdf



def pretreat( path : str, new_path : str ) -> None :
    '''
    Prétraite une bdd au format geojson. Renvoie un fichier csv. Ajoute l'aire, la latitude et la 
    longitude du centre des parkings, et supprime les informations non nécessaires. 
    
    Args :
        path : le chemin de la bdd initiale
        
    Returns :
        new_path : celui du résultat.
    '''
    gdf = gpd.read_file( path, engine='pyogrio', driver='GeoJSON', ignore_geometry=False )
    gdf = add_infos(gdf)
    gdf = only_large_parkings(gdf)
    gdf = delete_undesired_types(gdf)
    gdf = better_format_for_lat_and_long(gdf)
    gdf = change_columns_name(gdf)
    gdf.to_csv(path_or_buf=new_path, index_label='id')
