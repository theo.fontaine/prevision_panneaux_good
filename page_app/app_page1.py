from dash import html
import plotly.express as px
import pandas as pd
import json
from dash import dcc

# Chargement des données de contour de départements
with open("data/contour-des-departements.geojson", "r", encoding="utf-8") as file:
    geojson_data = json.load(file)

df = pd.read_csv('data/meteo/meteo.csv')  # bdd pour carte ensoleillement
fichier_csv = 'data/bdd_transfos.csv'
df2 = pd.read_csv(fichier_csv, encoding='latin-1', engine='python')  # bdd parkings

notes = dict(zip(df['code_dept'], df['score']))

#Création de la carte météo pour implanter des panneaux solaires
fig = px.choropleth_mapbox(
    geojson=geojson_data,
    locations=[feature["properties"]["code"] for feature in geojson_data["features"]],
    featureidkey="properties.code",
    color=[notes.get(feature["properties"]["code"], 0) for feature in geojson_data["features"]],
    hover_name=[feature["properties"]["nom"] for feature in geojson_data["features"]],
    color_continuous_scale="Viridis",
    mapbox_style="carto-positron",
    center={"lat": 46.603354, "lon": 1.888334},
    zoom=4,
    opacity=0.5,
    title="Carte des régions les plus favorables en terme de météo"
)
fig.update_coloraxes(colorbar_title="Note")

# Histogramme
histogram_figure = px.histogram(df2, x='surface_du_parking', nbins=200, log_y=True, title='Distribution de la taille des parkings')
histogram_figure.update_xaxes(title_text='Taille du parking (m²)')
histogram_figure.update_yaxes(title_text='Nombre (échelle logarithmique)')
histogram_figure.update_traces(marker_color='red')


# Assuming you want to count the number of rows for each 'code_dept'
bar_chart_figure = px.bar(df2, x='code_dept', title='Répartition du nombre de parkings par département',
                           category_orders={'code_dept': sorted(df2['code_dept'].unique())},
                           barmode='stack')
bar_chart_figure.update_xaxes(title_text='Département')
bar_chart_figure.update_yaxes(title_text='Nombre de parkings')
bar_chart_figure.update_traces(marker_color='green')
bar_chart_figure.update_layout(legend=dict(orientation="h", y=1.1, x=0.5))

# Texte affiché pour la présentation
ligne_1 = 'Bienvenue. Cet outil est destiné aux entreprises qui vendent des ombrières photovoltaïques. Il intervient pendant \
la phase de recherche de clients. Il propose les services suivants :'
ligne_2 = 'la collecte automatique de données sur les parkings qui peuvent potentiellement être équipés. Ces données sont la \
localisation, l\'aire, le nombre de jours de pluie moyen par an et éventuellement le nom du parking. Celles-ci sont \
téléchargeables (page \'Carte\') ;'
ligne_3 = 'la visualisation de ces données sur une carte (page \'Carte\') ;'
ligne_4 = 'une simulation des économies réalisées par an et par surface de panneaux installées à montrer au gestionnaire du parking (page \'Simulation\').'
ligne_5 = 'On remarque une décroissance exponentielle sur la distribution des parkings en fonction de leur taille.'

# Layout utilisé pour la page 1
tab_layout = html.Div([
    
    # Description
    html.H2('Projet Coding Weeks'),
    html.P(ligne_1),
    html.Li(ligne_2),
    html.Li(ligne_3),
    html.Li(ligne_4),
    
    # Carte ensoleillement
    dcc.Graph(figure=fig, style={'height':'530px'}),

    # Histogramme + commentaire
    dcc.Graph(id='histogram-size', figure=histogram_figure),
    html.P(ligne_5),

    # Graphique de répartition du nombre de parkings par département
    dcc.Graph(id='bar-chart', figure=bar_chart_figure),
])
