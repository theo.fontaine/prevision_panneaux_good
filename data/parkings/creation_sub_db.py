
import geopandas as gpd
import pandas as pd
import json
from data.parkings.pretreatment import pretreat
import os

def create_sdb( N : int, new_path : str, all_db = False ) -> None :
    '''
    Crée une sous-bdd de la bdd complète, avec N parkings. La stocke au chemin new_path.
    Peut aussi ouvrir toute la base. Inutile du coup, mais permet de toujours taper la même 
    fonction.
    
    Args : 
        N : Taille de la sous-bdd ;
        new_path : chemin où la sous-bdd doit être stockée ;
        all_db : si True, écrit dans new_path la bdd complète
    
    Returns :
        None
    '''
    path = 'data/parkings/parkings_complete.json'

    with open(path, encoding='utf-8',) as datafile:
        d = json.load(datafile)
    
    if not all_db:
        d['elements'] = d['elements'][0:N]
    
    with open(new_path, 'w') as f:
        json.dump(d,f, indent = 2)
    
def json_to_geojson( path : str, new_path : str ) -> None :
    '''
    Create a new file similar to the first with format geojson. Le format geojson est nécessaire pour utiliser
    la méthode area() du module geopandas pour calculer rapidement l'aire à partir des latitudes/longitudes des 
    sommets du parking. Une différence entre le format de sortie de cette fonction et le format geojson cependant :
    les sommets des polygônes ne sont pas nécessairement donnés dans le sens direct. Je ne l'ai pas implémenté 
    car ce serait long, et ça ne pose pas de problème dans la suite. 
    
    Args :
        path : chemin où se situe la dbb au format json à transformer ;
        new_path : chemin où écrire la dbb transformée au format geojson.
    
    Returns :
        None
    '''
    
    data = json.load(open(path))
    
    new_data = create_geojson(data)

    output = open(new_path, 'w')
    json.dump(new_data, output, indent=2)


def create_geojson( data : dict ) -> dict :
    '''
    Sous_fonction de json_to_geojson. Construit la nouvelle structure au format geojson.
    
    Args :
        data : la bdd au format json
    
    Returns :
        new_data : la bdd au format geojson
    '''
    
    new_data = {
        "type" : "FeatureCollection",
    "features" : []
    }
    
    for par in data['elements']:
        
        # Important, la bdd initiale contient des coquilles, des parkings dont les sommets ne bouclent pas.
        # Ca génère des erreurs après, donc il faut le corriger. En fait, cette condition fait partie du format
        # geojson.
        first_sommet = par['geometry'][0]
        last_sommet = par['geometry'][-1]
        
        if first_sommet == last_sommet:
            
            res = {
                "type" : "Feature",
                "properties" : select_tags( par ),
                "geometry" : {
                    "type" : "Polygon",
                    "coordinates" : [
                        [ [ sommet['lat'], sommet['lon'] ] for sommet in reversed(par['geometry']) ]
                    ]
                }
            }
            
            new_data["features"].append( res )
    
    return new_data

def select_tags( par : dict ) -> dict :
    '''
    Renvoie l'ensemble des tags du parking intéressants. Il faut le faire maintenant pour réduire le 
    temps de calcul. Les tags considérés intéressants sont définis par la variable "interesting_tags". Ce sont :
    - 'access' : caractère public/privé/pour les clients du parking
    - 'parking' : valeurs possibles : sousterrain, en surface, à étages
    - 'geometry' : la latitude et longitude des sommets de la frontière du parking
    - 'name'  : /   
    
    Args :
        par : la représentation d'un parking
    
    Returns :
        selected_tags : les tags intéressants.
    '''
    selected_tags = {}
    interesting_tags = ['access', 'name', 'parking', 'geometry']
    
    for tag in par['tags']:
        if tag in interesting_tags:
            selected_tags[ tag ] = par['tags'][ tag ]
    
    return selected_tags
    
    
def create_treated_sub_db( N : int, all_db = False ) -> None :
    '''
    Crée une sous-base de la base complète, avec moins de N parkings, ayant subi les traitements suivants :
    - que certaines colonnes de conservées 
    - d'autres rajoutées (aire, latitude et longitude du centre)
    - les parkings de taille inférieure à 1500 m^2 enlevés
    - au format csv
    - les parkings sousterrains enlevés, où dont le type n'est pas spécifié
    
    Args :
        N : taille de la bdd initiale. La base de données de sortie est plus petite, car tous les parkings 
        ne vérifient pas les conditions (ex : certains sont souterrains).
        all_db : Si vraie, la fonction utilise en entrée la bdd non traitée complète.
        
    Returns :
        None
    '''
    path_1 = 'data/parkings/parkings_ssb.json'
    create_sdb(N, path_1, all_db )

    path_2 = 'data/parkings/parkings_ssb.geojson'
    json_to_geojson(path_1, path_2)

    path_3 = 'data/parkings/parkings_sdb_treated.csv'
    pretreat(path_2, path_3)

    os.remove(path_1)
    os.remove(path_2)   