import geopandas as gpd
from shapely.geometry import Point

# Charger les données géospatiales des départements français
departements = gpd.read_file("contour-des-departements.geojson")

def trouver_departement(lat: float, lon: float):
    """Renvoie le département correspondant aux coordonnées s'il est trouvé
       Renvoie None sinon"""
    # Créer un point à partir des coordonnées
    point = Point(lon, lat)
    
    # Parcourir les départements pour trouver lequel contient le point
    for index, row in departements.iterrows():
        if row['geometry'].contains(point):
            return row['code']
    return None
