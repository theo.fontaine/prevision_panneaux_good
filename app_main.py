from dash import Dash, html, dcc
import plotly.express as px
import pandas as pd
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
import shutil

# Import des modules
from carte.csv_to_cartehtml import converti_bdd_carte
from page_app import app_page1, app_page2, app_page3

# Titre + Menu
app = Dash(__name__, suppress_callback_exceptions=True, external_stylesheets=["assets/styles.css"])

def initialize_layout():
    ''' 
    Fonction dont le but est de décrire l'architecture de la page principale 
    '''
    return html.Div(children=[
        html.H1("Twitter_panneaux", style={'text-align': 'center'}),
        dcc.Tabs(id='tabs', value='tab1',  children=[
            dcc.Tab(label='Présentation', value='tab1', style={'backgroundColor': '#BDBDBD', 'color': 'white'}),
            dcc.Tab(label='Carte', value='tab2', style={'backgroundColor': '#BDBDBD', 'color': 'white'}),
            dcc.Tab(label='Simulation', value='tab3', style={'backgroundColor': '#BDBDBD', 'color': 'white'}),
        ]),
        html.Div(id='tabs-content'),
    ])
    
app.layout = initialize_layout()


# Définition des contenus des onglets à partir des pages
tab_contents = {
    'tab1': app_page1.tab_layout,
    'tab2': app_page2.tab_layout,
    'tab3': app_page3.tab_layout,
}

@app.callback(Output('tabs-content', 'children'),
              [Input('tabs', 'value')])



def update_tab_content(selected_tab):
    ''' 
    Fonction pour rafraîchir la page selon l'onglet sur lequel l'utlisateur intéragit 
    '''
    return tab_contents[selected_tab]


#######
# Mise à jour la carte en fonction des curseurs (et de l'onglet)
######
@app.callback(Output('map', 'srcDoc'),
              [Input('critere_ensoleillement', 'value'),
               Input('taille_parking_input', 'value'),
               Input('toggle-button-transfos', 'value')],
              [State('tabs', 'value')])
def update_map(critere_ensoleillement, taille_parking, avec_transfo, selected_tab):
    '''Permet d'acutaliser la carte dès qu'un changement des critères est effectué'''
    if selected_tab == 'tab2':
        converti_bdd_carte(critere_ensoleillement,taille_parking, avec_transfo)
        return open('carte/carte_live.html', 'r').read()
    else:
        return ''  # Retourne une chaîne vide pour les onglets autres que 'tab2'

#######
# Mise à jour des données téléchargée par le bouton download
#######
@app.callback(
    Output("download-csv", "data"),
    [Input("btn-download-csv", "n_clicks")],
    [State("critere_ensoleillement", "value"),
     State("taille_parking_input", "value"),
     State('toggle-button-transfos', 'value')],
    prevent_initial_call=True,)
def download_csv(n_clicks, seuil_meteo, seuil_surface_parking, avec_transfos):
    '''télécharge le fichier des données filtré'''
    if n_clicks is None:
        raise PreventUpdate
    fichier_csv = 'data/bdd_transfos.csv'
    dataframe = pd.read_csv(fichier_csv)

    # Filtrages des données en fonction des curseurs
    if(avec_transfos):
        centres_filtrés = dataframe[
            (dataframe['meteo'] >= seuil_meteo) &            
            (dataframe['surface_du_parking'] >= seuil_surface_parking) &
             dataframe['transfos']
        ]
    else:
        centres_filtrés = dataframe[
            (dataframe['meteo'] >= seuil_meteo) &            
            (dataframe['surface_du_parking'] >= seuil_surface_parking)
        ]

    # Création fichier csv filtré
    temp_csv = centres_filtrés.to_csv(index=False, encoding='utf-8')
    
    return dcc.send_data_frame(
        centres_filtrés.to_csv,
        filename="centres_commerciaux_filtré.csv",
        index=False
    ) 

#######
# Permet d'afficher le coût du projet (page2)
#######
@app.callback(Output('budget-output', 'children'),
             [Input('taille_parking_input', 'value')])
def update_budget_output(taille_parking):
    '''Affiche le coût'''
    budget = taille_parking * 1000 # Conversion de la surface en euros (surface * 1000)
    formatted_budget = "{:,.2f}".format(budget)  #ajouter une virgule comme séparateur des milliers et afficher deux décimales
    return f'Coût minimum du projet : {formatted_budget} €'
    
#######
# Permet d'afficher le nbre de parking en fonctions des critères
#######
@app.callback(Output('nb-parkings-affiches', 'children'),
              [Input('critere_ensoleillement', 'value'),
               Input('taille_parking_input', 'value'),
               Input('toggle-button-transfos', 'value')],)
def afficher_nombre_parkings(seuil_meteo, seuil_surface_parking, avec_transfos):
    '''Affiche le nombre de parking vérifiant ce qui est demandé'''
    dataframe = pd.read_csv("data/bdd_transfos.csv")
    if(avec_transfos):
        centres_filtres = dataframe[
            (dataframe['meteo'] >= seuil_meteo) &            
            (dataframe['surface_du_parking'] >= seuil_surface_parking) &
            dataframe['transfos']
        ]
    else:
        centres_filtres = dataframe[
            (dataframe['meteo'] >= seuil_meteo) &            
            (dataframe['surface_du_parking'] >= seuil_surface_parking)
        ]
    nb_parkings = len(centres_filtres)
    return f"Il y a {nb_parkings} parkings qui répondent aux critères"


#######
# Affiche l'économie faites en fonction du département sur la page 3
#######
@app.callback(
    Output('output-note', 'children'),
    Input('input-department', 'value')
)
def update_output(department : int):
    '''Affiche l'économie réalisé page3'''
    df = pd.read_csv('data/meteo/meteo.csv')
    if department is not None and department != '':
        department = str(department) 
        row = df[df['code_dept'] == department]
        
        if not row.empty:
            note = row['score'].values[0]
            return html.Div([
                html.P(f"La puissance produite par 1 m2 de panneau solaire dans le département {(department)} sur une année est : {int(note)*170} kWh."),
                html.P(f"Or, le prix EDF au kWh est de 0,23€."),
                html.P(f"Vous auriez fait une économie de {round(int(note)*170*0.23, 3)} € en 1 année par m2.")
            ])
        else:
            return f"Aucune note trouvée pour le département {department}"
    else:
        return ""

# Lancement app
def run_app():
    app.run(debug=True)

if __name__ == '__main__':
    app.run(debug=True)
