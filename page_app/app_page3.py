#Import des modules/bibliothèques
import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import pandas as pd

#Layout utilisé pour la page 3
tab_layout = html.Div([
    html.H1("Que gagne l'entreprise ?"),

    dcc.Input(id='input-department', type='text', placeholder='Entrez votre numéro de département'),
    
    html.Div(id='output-note', style={'text-align': 'center', 'font-size': '18px', 'margin-top': '20px'})
])
