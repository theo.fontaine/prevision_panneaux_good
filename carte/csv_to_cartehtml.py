#Import des modules/bibliothèques
import folium
import pandas as pd
from folium.plugins import MarkerCluster
import numpy as np

def converti_bdd_carte(seuil_meteo, seuil_surface_parking, avec_transfo):
    '''Permet de convertir le fichier de donnée csv en une carte qui place les points des parkings en fonctions des critères choisi'''
    
    fichier_csv = 'data/bdd_transfos.csv'
    dataframe = pd.read_csv(fichier_csv, encoding='latin-1', engine='python')


    # Si None pour taille parking
    if seuil_surface_parking is None:
        seuil_surface_parking = 0


    # Filtrage en fonction des critères et des limites de la carte
    if(avec_transfo):
        centres_filtrés = dataframe[
            (dataframe['meteo'] >= seuil_meteo) &            
            (dataframe['surface_du_parking'] >= seuil_surface_parking) &
            dataframe['transfos']
        ]
    else:
        centres_filtrés = dataframe[
            (dataframe['meteo'] >= seuil_meteo) &            
            (dataframe['surface_du_parking'] >= seuil_surface_parking)
        ]

    # Convertion en dico
    centres_commerciaux = centres_filtrés.to_dict(orient='records')

    # Création carte (centrée à Paris)
    ma_carte = folium.Map(location=[46.603354, 2.457622], zoom_start=5)

    marker_cluster = MarkerCluster().add_to(ma_carte)

    # Ajout des points sur la carte
    for centre in centres_commerciaux:
        # Couleur du marqueur en fonction de "presence_transfo" (vert si transfo sinon rouge)
        if str(centre["nom"]) != "nan":
            txt_popup = f"<strong>{centre['nom']}</strong> \n superficie du parking : {centre['surface_du_parking']:.0f}m²"
            folium.Marker(
                location=[centre["lat"], centre["lon"]],
                popup=txt_popup,
                tooltip=centre["nom"],
            ).add_to(marker_cluster)
        else:
            txt_popup = f"superficie du parking : {centre['surface_du_parking']:.0f}m²"
            folium.Marker(
                location=[centre["lat"], centre["lon"]],
                popup=txt_popup,
            ).add_to(marker_cluster)
        

    # Enregistrez la carte dans un fichier HTML
    ma_carte.save("carte/carte_live.html")
