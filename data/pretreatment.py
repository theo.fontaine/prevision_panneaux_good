import pandas as pd
import random

from meteo.trouver_region import trouver_departement


def create_dict_meteo(path_meteo: str):
    """A partir du fichier .csv meteo cree un dictionnaire associant à un departement son score meteo"""
    meteo = pd.read_csv(path_meteo)
    return dict(zip(meteo['code_dept'], meteo['score']))


def add_meteo(path_parkings: str, path_meteo: str):
    """Ajoute au dataframe des parkings la meteo associée et l'enregistre dans un .csv"""
    parkings = pd.read_csv(path_parkings)
    dict_meteo = create_dict_meteo(path_meteo)

    # Supprimer les lignes avec 'lon' ou 'lat' manquants
    parkings = parkings.dropna(subset=['lon', 'lat'])

    # Obtenir le code_dept pour les lignes restantes
    parkings['code_dept'] = parkings.apply(lambda row: trouver_departement(row['lat'], row['lon']), axis=1)

    # Filtrer les lignes où code_dept est manquant
    parkings = parkings.dropna(subset=['code_dept'])

    # Utiliser map pour assigner les valeurs de meteo en utilisant le code_dept
    parkings['meteo'] = parkings['code_dept'].map(dict_meteo)

    parkings.to_csv("parkings_avec_meteo.csv", index=False)  # Sauvegarder directement le fichier .csv sans retourner le DataFrame


def reduire_bdd(df: pd.DataFrame):
    """supprime les parkings dont la surface est dans les 95% plus petites"""
    q3 = df['surface_du_parking'].quantile(0.95)
    df_reduit = df[df['surface_du_parking']>=q3]
    return df_reduit


def ajout_transfos(df: pd.DataFrame, proba=0.7):
    """ajout des valeurs de transfos de manière aléatoire"""
    df['transfos'] = [random.random() < proba for _ in range(len(df))]#false avec une probabilité = proba
    return df


if __name__ == "__main__":
    parkings = pd.read_csv("parkings_avec_meteo_reduit.csv")
    parkings = ajout_transfos(parkings)
    parkings.to_csv("bdd_transfos.csv", index=False)
